exports.bfs=function(graph, startx, starty, dist, endx, endy, prevNode){
    var dx = [0,0,1,-1];
    var dy = [1,-1,0,0];
    var queue=[];
    queue.push(graph[starty][startx]);
    graph[starty][starty].dist=0;
    var prevNode=null;
    while(queue.length>0){
        var cur=queue.pop();
        if(cur==graph[endy][endx]){
            return;
        }
        for(var i=0; i<4; i++){
            if(dy[i]+cur.y>=graph.length||dx[i]+cur.x>=600||dy[i]+cur.y<0||dx[i]+cur.x<0)break;
        if(graph[dy[i]+cur.y][dx[i]+cur.x]!=null){
            if(!graph[dy[i]+cur.y][dx[i]+cur.x].visited){
                graph[dy[i]+cur.y][dx[i]+cur.x].dist=cur.dist+1;
                graph[dy[i]+cur.y][dx[i]+cur.x].visited=true;
                graph[dy[i]+cur.y][dx[i]+cur.x].prevNode=cur;
                queue.push(graph[dy[i]+cur.y][dx[i]+cur.x]);
            }
        }
        }
    }
}