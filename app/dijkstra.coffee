module.exports = class Dijkstra
  constructor:()->
  dijkstra:(start, end)->
    queue=[]
    start.dist=0
    queue.push(start)
    while queue.size()>0
      cur=queue.pop()
      dist=cur.dist
      if cur == end then break
      for i in cur.neighbors
        neighbor=cur.neighbors[i].node
        edgeweight=cur.neighbors[i].dist
        newDist = dist + edgeweight
        if neighbor.dist == null or neighbor.dist > newDist
          neighbor.dist=newDist
          queue.push(neighbor)
        