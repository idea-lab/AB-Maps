modules.exports= bfs = (graph, startx, starty, dist, endx, endy, prevNode) ->
    dx = [0,0,1,-1]
    dy = [1,-1,0,0]
    queue=[]
    queue.push(graph[starty][startx])
    graph[starty][starty].dist=0
    prevNode=null
    while queue.length>0
        cur=queue.pop()
        if cur==graph[endy][endx] then return
        for i in [0...4]
            if dy[i]+cur.y>=graph.length||dx[i]+cur.x>=600||dy[i]+cur.y<0||dx[i]+cur.x<0 then break
        if(graph[dy[i]+cur.y][dx[i]+cur.x]!=null)
            if(!graph[dy[i]+cur.y][dx[i]+cur.x].visited)
                graph[dy[i]+cur.y][dx[i]+cur.x].dist=cur.dist+1
                graph[dy[i]+cur.y][dx[i]+cur.x].visited=true
                graph[dy[i]+cur.y][dx[i]+cur.x].prevNode=cur
                queue.push(graph[dy[i]+cur.y][dx[i]+cur.x])