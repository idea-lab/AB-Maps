originx = 0
originy = 0
destx = 0
desty = 0
lastNode = null
clickhandler = 0
mapNode = require('mapNode')
$('canvas').on 'click', (e)->
	if(clickhandler == 0) 
		originx = e.pageX-334
		originy = e.pageY-160
	if(clickhandler==1)	
		destx = e.pageX-334
		desty = e.pageY-160
		bfs=require('bfs')
		graph=require('graph') 
		for i in [0...graph.length]
			for j in [0...graph[i].length]
				graph[i][j]=new mapNode(graph[i][j], j, i)
		bfs.bfs(graph, originx, originy, 0, destx, desty, null)
		document.getElementById("dist").innerHTML=graph[desty][destx].dist
		for i in [0...graph.length]
			for j in [0...graph[i].length]
			  graph[i][j]=require('map')[i][j]
	clickhandler=if clickhandler==0 then 1 else 0
tracer = (lastNode) ->
	xArray = []
	yArray = []
	i = 0
	if (lastNode==null) then return
	while (lastNode!=null)
		xArray[i] = lastNode.x
		yArray[i] = lastNode.y
		i++ 
		if (lastNode.prevNode!=null) then lastNode = lastNode.prevNode
	return [xArray, yArray]
